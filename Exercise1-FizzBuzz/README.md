# FizzBuzz
Write a program that prints the numbers from 1 to X. X is a value entered by a user on
the Qt window. But for multiples of three print “Fizz” instead of the number and for the
multiples of five print “Buzz”. For numbers which are multiples of both three and five
print “FizzBuzz “.

For this exercise the window has already been created and the widgets connects. The task
is to complete the implementation within the **OnCalculateButtonClicked()** function in
_FizzBuzz.cpp_.

In addition unit tests have been provided. When the project is first cloned some of the unit tests
should fail. Once the unit tests pass your project should work as intended. The tests should run
automatically during a build of the project "FizzBuzz". If you want to run the unit tests on their
own you can run the "UnitTests" project.

## Documentation
The following API documentation may be useful.
- [QString class](https://doc.qt.io/qt-5.15/qstring.html)
- [QStringList class](https://doc.qt.io/qt-5.15/qstringlist.html)

## Example Output
See _FizzBuzz/Resources/FizzBuzzWithQt.mp4_ for a video demonstration.

![](Resources/MainWindow.png)