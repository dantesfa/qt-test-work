# QtWorkshop
This repository serves as a set of Qt exercises for practicing Qt, C++, and object oriented
development. To use this repo create a fork. Each exercise folder has a README inside that
explains what to do for the exercise.

## Exercises
This is a list of the exercises in the order they are intended to be completed in. Details for
each exercise can be found in its folders README.
- Exercise 1: FizzBuzz
- Exercise 2: Alphabet Cipher
- Exercise 3: FizzBuzz Expanded

If you get stuck or want to compare your solution after you complete it to a different solution,
the repository has an "ExampleSolutions" branch that contains completed exercises.

## Installing QtCreator
If you do not have QtCreator an open source version can be installed [from the Qt website](https://www.qt.io/product/development-tools). This should be able to be installed on any operating system.

1. Click the "Download. Try." button.

![](Resources/QtDownloadButton.png)

2. On the next page use the link to the open source installer in the upper right.

![](Resources/QtOpenSourceLink.png)

3. Scroll down to the "Download the Qt Online Installer" button to download the installer.

![](Resources/QtOnlineInstallerButton.png)

4. Open the Qt Installer. You will need an account to continue. Use the Sign Up link to create
one if you do not have one.

![](Resources/QtInstallerLogin.png)

5. Navigate through the steps as desired. Be sure to understand the the Open Source Obligations
if you wish to publish anything using this installation. When choosing the installation folder,
selected custom install.

![](Resources/QtInstallerCustomInstallation.png)

6. Hit next and select the MinGW installation for your desired version. Any non-beta version should
be acceptable, but the exercises were written using 5.15. A version of this installer can be opened
from Qt Creator later to add modules if more or different tools or versions are desired. The MinGW
version is the minimum needed to perform the exercises in this workshop.

![](Resources/QtInstallerMinGW.png)

## Opening Projects
The exercises in this workshop utilize qMake. Open them by opening the top level *.pro file for
the Exercise.

![](Resources/ExerciseFolderStructureExample.png)

The first time you open a project you should be prompted to select a kit to configure the project
for. This sets up the version of Qt to build with.

![](Resources/ConfigureProjectScreen.png)

## Running a Project
These projects are setup to build into debug. Once built, you select the project to run in the
bottom left and click the play button.

![](Resources/RunSelection.png)
