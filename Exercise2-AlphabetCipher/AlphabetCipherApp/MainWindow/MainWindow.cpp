#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow()
    : Ui(new Ui::MainWindow)
{
    Ui->setupUi(this);
    SetUpHeaders();
    //TODO: Connect encrypt and decrypt buttons to respective signals
}

MainWindow::~MainWindow()
{
    delete Ui;
}

void MainWindow::SetOutputString(const QString &message)
{
    Ui->OutputMessageLabel->setText(message);
}

void MainWindow::OnEncryptButtonClicked()
{
    //TODO: emit EncryptButtonClicked signal
}

void MainWindow::OnDecryptButtonClicked()
{
    //TODO: emit DecryptButtonClicked signal
}

void MainWindow::SetUpHeaders()
{
    QFont font("Arial", 12, QFont::Bold);
    SetUpKeywordHeader(font);
    SetUpInputHeader(font);
    SetUpOutputHeader(font);
}

void MainWindow::SetUpKeywordHeader(QFont &font)
{
    Ui->KeywordHeaderLabel->setFont(font);
    Ui->KeywordHeaderLabel->setText(QString("Keyword"));
}

void MainWindow::SetUpInputHeader(QFont &font)
{
    Ui->InputHeaderLabel->setFont(font);
    Ui->InputHeaderLabel->setText(QString("Input Text (No Spaces)"));
}

void MainWindow::SetUpOutputHeader(QFont &font)
{
    Ui->OutputHeaderLabel->setFont(font);
    Ui->OutputHeaderLabel->setText(QString("Encrypted Text"));
}

