# FizzBuzz
For this exercise the window has already been created and the Alphabet Cipher kata is implemented
in the AlphabetCipher class. The task is to connect the encrypt and decrypt buttons in
*MainWindow.cpp* to their corresponding slots **OnEncryptButtonClicked** and
**OnDecryptButtonClicked**. Then in those slots emit the signals as needed (see
*MainWindowInterface.h* for the signal declarations). The AlphabetCipher class is already
connected to this signals.

In addition unit tests have been provided. When the project is first cloned some of the unit tests
should fail. Once the unit tests pass your project should work as intended. The tests should run
automatically during a build of the project "AlphabetCipher". If you want to run the unit tests
on their own you can run the "UnitTests" project.

## Documentation
The following API documentation may be useful.
- [Signals and Slots](https://doc.qt.io/qt-6/signalsandslots.html)
- [Connect Function](https://doc.qt.io/qt-6/qobject.html#connect-3)
- [QLineEdit](https://doc.qt.io/qt-6/qlineedit.html)
- [QPushButton](https://doc.qt.io/qt-6/qpushbutton.html)

## Example Output
See _Resources/AlphabetCipherWithQt.mp4_ for a video demonstration.

![](Resources/EncryptionExample.png)