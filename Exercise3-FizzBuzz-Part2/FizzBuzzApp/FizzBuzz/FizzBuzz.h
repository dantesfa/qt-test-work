#ifndef FIZZBUZZ_H
#define FIZZBUZZ_H

#include "MainWindow/MainWindowInterface.h"
#include <QString>

class FizzBuzz : public QObject
{
    Q_OBJECT
public:
    FizzBuzz(MainWindowInterface& window);
    void Run();

private slots:
    void OnCalculateButtonClicked(const int count);

private:
    MainWindowInterface *Window;
};

#endif // FIZZBUZZ_H
