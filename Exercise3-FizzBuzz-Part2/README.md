# FizzBuzz
For this exericise you'll expand what was done in exercise 1. Write a program that prints the
numbers from 1 to X. X is a value entered by a user on the Qt window. But for multiples of A print
“Fizz” instead of the number and for the multiples of B print “Buzz”. For numbers which are
multiples of both A and B print "FizzBuzz". Instead of three and five as in exercise one, this time
A and B should be entered by a user via the main window.

The template code is the same as exercise 1. Your task is to add entry boxes to MainWindow.ui and
connect them to the FizzBuzz algorithm. There are unit tests for this connection that assume
an interface where the use inputs are passed along with the count value in the
OnCalculateButtonClicked() signal. You are welcome to change this architecture if desired, though
you should consider updating tests accordingly.

Since users are now inputing a value that is likely being divided by, remember to consider what
should happen if the user enters 0. There are *not* unit tests for this scenario. There are many
ways you can sanitize this input. That task is left up to you.

## Example Output
![](Resources/MainWindow.png)